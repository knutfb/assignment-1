let outstandingLoan = 0.0;
let bankBalance = 0.0;
let moneyEarned = 0.0;
let hasLoan;
let currentLaptop;
const interestValue = 0.9;

// Buttons
const getLoanButton = document.getElementById("getLoanButton");
const buyComputerButton = document.getElementById("buyComputerButton");
const repayLoanButton = document.getElementById("repayLoanButton");
const bankButton = document.getElementById("bankButton");
const workButton = document.getElementById("workButton");

// Span tags
const computersSpecsTagElement = document.getElementById("computerSpecsTag");
const computersImageTagElement = document.getElementById("computerImageTag");
const saleryTagElement = document.getElementById("payTag");
const balanceTagElement = document.getElementById("balanceTag");
const debtTagElement = document.getElementById("debtTag");
const computerPriceTagElement = document.getElementById("computerPriceTag");
const computerTitleTagElement = document.getElementById("computerTitleTag");
const computersDescriptionTagElement = document.getElementById(
  "computerDescriptionTag"
);

// Select
const computersSelectElement = document.getElementById("computerSelect");

workButton.addEventListener("click", function () {
  moneyEarned += 100;
  renderElements();
});

// Only displaying Repay loan button when loan is active
hasLoan
  ? (repayLoanButton.style.display = "block")
  : (repayLoanButton.style.display = "none");

// Event listeners for button for getting a loan, transering to bank and buying computer
getLoanButton.addEventListener("click", function () {
  let loan_amount = parseInt(
    Math.abs(prompt("How much would you like to loan?"))
  );
  if (eligibleForLoan(loan_amount) && !Number.isNaN(loan_amount)) {
    hasLoan = true;
    outstandingLoan += loan_amount;
    renderElements();
  }
});

bankButton.addEventListener("click", function () {
  transferToBank(moneyEarned);
  renderElements();
});

buyComputerButton.addEventListener("click", function () {
  buyLaptop(currentLaptop);
  renderElements();
});

// Transfers to bank from salary account
function transferToBank(amount) {
  if (hasLoan) {
    console.log(outstandingLoan <= amount * (1 - interestValue));
    console.log(amount * (1 - interestValue));
    if (outstandingLoan <= amount * (1 - interestValue) && hasLoan) {
      bankBalance += amount - outstandingLoan;
      outstandingLoan = 0;
      hasLoan = false;
    } else {
      moneyEarned -= amount;
      bankBalance += amount * interestValue;
      outstandingLoan -= amount * 0.1;
    }
  } else {
    moneyEarned -= amount;
    bankBalance += amount;
  }
}

// User buys laptop with money from bank account
function buyLaptop() {
  computerPrice = computers[currentLaptop - 1].price;
  if (bankBalance >= computerPrice) {
    bankBalance -= computerPrice;
    alert("Congratulations!");
  } else {
    alert("You are unable to purchase this computer");
  }
}

// Event listener for button for repaying loan
repayLoanButton.addEventListener("click", function () {
  if (moneyEarned >= outstandingLoan && hasLoan) {
    bankBalance += moneyEarned - outstandingLoan;
    moneyEarned -= outstandingLoan;
    moneyEarned = 0;
    outstandingLoan = 0;
    hasLoan = false;
  } else {
    outstandingLoan -= moneyEarned;
    moneyEarned = 0;
  }
  renderElements();
});

// Checks us one is eligeble for receiving loan
function eligibleForLoan(amount) {
  if (!hasLoan && amount <= 2 * bankBalance) {
    return true;
  } else if (hasLoan) {
    alert("You already have a loan that needs to be payed.");
    return false;
  } else {
    alert(
      "The loan you requested infringes with our company policy. Please send us an email in case of further questions."
    );
    return false;
  }
}

// API call to fetch computers and population of dropdown
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToDropdown(computers));

const addComputersToDropdown = (computers) => {
  computers.forEach((x) => {
    addComputerToDropdown(x);
  });
  setTagValues(computers[0]);
  currentLaptop = computers[0].id;
};

const addComputerToDropdown = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersSelectElement.appendChild(computerElement);
  computerElement.classList.add("dropdown");
};

const handleComputerMenuChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  currentLaptop = selectedComputer.id;
  setTagValues(selectedComputer);
};

computersSelectElement.addEventListener("change", handleComputerMenuChange);

function setTagValues(selectedComp) {
  computerPriceTagElement.innerText = selectedComp.price + " kr";
  computersDescriptionTagElement.innerText = selectedComp.description;
  computersSpecsTagElement.innerText = selectedComp.specs.join("\n");
  computerTitleTagElement.innerText = selectedComp.title;
  computersImageTagElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + selectedComp.image;
}

function renderElements() {
  hasLoan
    ? (repayLoanButton.style.display = "block")
    : (repayLoanButton.style.display = "none");
  saleryTagElement.innerHTML = moneyEarned + " kr";
  debtTagElement.innerHTML = outstandingLoan + " kr";
  balanceTagElement.innerHTML = bankBalance + " kr";
}
