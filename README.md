## README

## Summary

This assignment is part of the Accelerate program at Noroff.

The program lets a user work and accumulate money, which can be transferred to a bank. Here, the user may receive a loan, which cannot exceed twice the amount of the salery-account.
The user can also browse computers which can be purchased with the amount found in its bank balance.

## Technology

```sh
HTML, Javascript, Bootstrap v5
```

## Usage

```sh
Run the project with npm install
```
